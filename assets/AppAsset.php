<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'frontend/css/bootstrap.min.css',
        'frontend/css/plugins.css',
        'frontend/css/main.css',
        'frontend/css/themes.css',
    ];
    public $js = [
        'frontend/js/vendor/jquery.min.js',
        'frontend/js/vendor/bootstrap.min.js',
        'frontend/js/plugins.js',
        'frontend/js/app.js'
    ];

    public $depends = [

    ];

}
