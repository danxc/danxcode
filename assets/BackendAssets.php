<?php
/**
 * Created by PhpStorm.
 * User: danilo
 * Date: 12/11/18
 * Time: 12:37
 */

namespace app\assets;


use yii\web\AssetBundle;

class BackendAssets extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

    ];
    public $js = [
    ];
    public $depends = [

    ];
}