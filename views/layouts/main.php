<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Desenvolvimento de Sites e soluções web, mobile e embarcados em Salvador">
    <meta name="author" content="daniloxcode">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">


    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="img/favicon.png">
    <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
    <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
    <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
    <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
    <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
    <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
    <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
    <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
    <!-- END Icons -->


    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src="frontend/js/vendor/modernizr.min.js"></script>
</head>
<body>
<?php $this->beginBody() ?>
<!-- Page container-->
<div id="page-container">
    <!-- Site Header -->
    <header>
        <div class="container">
            <!-- Site Logo -->
            <a href="index.html" class="site-logo">
                <i class="gi gi-flash"></i> <strong>DANX</strong>CODE
            </a>
            <!-- Site Logo -->

            <!-- Site Navigation -->
            <nav>
                <!-- Menu Toggle -->
                <!-- Toggles menu on small screens -->
                <a href="javascript:void(0)" class="btn btn-default site-menu-toggle visible-xs visible-sm">
                    <i class="fa fa-bars"></i>
                </a>
                <!-- END Menu Toggle -->

                <!-- Main Menu -->
                <ul class="site-nav">
                    <!-- Toggles menu on small screens -->
                    <li class="visible-xs visible-sm">
                        <a href="javascript:void(0)" class="site-menu-toggle text-center">
                            <i class="fa fa-times"></i>
                        </a>
                    </li>
                    <!-- END Menu Toggle -->
                    <li class="active">
                        <a href="javascript:void(0)" class="site-nav-sub"><i class="fa fa-angle-down site-nav-arrow"></i>Home</a>
                        <ul>
                            <li>
                                <a href="index.html" class="active">Full Width</a>
                            </li>
                            <li>
                                <a href="index_alt.html">Full Width (Dark)</a>
                            </li>
                            <li>
                                <a href="index_parallax.html">Full Width Parallax</a>
                            </li>
                            <li>
                                <a href="index_video.html">Full Width Video</a>
                            </li>
                            <li>
                                <a href="index_boxed.html">Boxed</a>
                            </li>
                            <li>
                                <a href="index_boxed_alt.html">Boxed (Dark)</a>
                            </li>
                            <li>
                                <a href="index_boxed_parallax.html">Boxed Parallax</a>
                            </li>
                            <li>
                                <a href="index_boxed_video.html">Boxed Video</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="site-nav-sub"><i class="fa fa-angle-down site-nav-arrow"></i>Páginas</a>
                        <ul>
                            <li>
                                <a href="blog.html">Blog</a>
                            </li>
                            <li>
                                <a href="blog_post.html">Blog Post</a>
                            </li>
                            <li>
                                <a href="portfolio_4.html">Portfolio 4 Columns</a>
                            </li>
                            <li>
                                <a href="portfolio_3.html">Portfolio 3 Columns</a>
                            </li>
                            <li>
                                <a href="portfolio_2.html">Portfolio 2 Columns</a>
                            </li>
                            <li>
                                <a href="portfolio_single.html">Portfolio Single</a>
                            </li>
                            <li>
                                <a href="team.html">Time</a>
                            </li>
                            <li>
                                <a href="helpdesk.html">Helpdesk</a>
                            </li>
                            <li>
                                <a href="jobs.html">Trabalhos</a>
                            </li>
                            <li>
                                <a href="how_it_works.html">Orçamento</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="features.html">Projetos</a>
                    </li>
                    <li>
                        <a href="pricing.html">Sites</a>
                    </li>
                    <li>
                        <a href="contact.html">Contato</a>
                    </li>
                    <li>
                        <a href="about.html">Sobre</a>
                    </li>
                    <li>
                        <a href="login.html" class="btn btn-primary">Log In</a>
                    </li>
                </ul>
                <!-- END Main Menu -->
            </nav>
            <!-- END Site Navigation -->
        </div>
    </header>
    <!-- END Site Header -->
    <?=$content?>
    <!-- Footer -->
    <footer class="site-footer site-section">
        <div class="container">
            <!-- Footer Links -->
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <h4 class="footer-heading">Sobre nós</h4>
                    <ul class="footer-nav list-inline">
                        <li><a href="contact.html">Contato</a></li>
                        <li><a href="contact.html">Suporte</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3">
                    <h4 class="footer-heading">Portifólio</h4>
                    <ul class="footer-nav list-inline">
                        <li><a href="javascript:void(0)">Site um</a></li>
                        <li><a href="javascript:void(0)">Site dois</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3">
                    <h4 class="footer-heading">Siga-nos</h4>
                    <ul class="footer-nav footer-nav-social list-inline">
                        <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-dribbble"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-rss"></i></a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3">
                    <h4 class="footer-heading"> &copy; <a href="">Daniloxcode</a></h4>
                    <ul class="footer-nav list-inline">
                        <li>Criado com <i class="fa fa-heart text-danger"></i> by <a href="http://goo.gl/vNS3I">Daniloxcode</a></li>
                    </ul>
                </div>
            </div>
            <!-- END Footer Links -->
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- End Page container-->
<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
