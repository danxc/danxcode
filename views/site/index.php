<?php

/* @var $this yii\web\View */

$this->title = 'Daniloxcode | Crie seu site/solução personalizada - Salvador/Bahia';
?>

<!-- Home Carousel -->
<div id="home-carousel" class="carousel carousel-home slide" data-ride="carousel" data-interval="5000">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="active item">
            <section class="site-section site-section-light site-section-top themed-background-default">
                <div class="container">
                    <h1 class="text-center animation-slideDown hidden-xs"><strong>A complete web solution for your awesome project</strong></h1>
                    <h2 class="text-center animation-slideUp push hidden-xs">Bring your project to life months sooner</h2>
                    <p class="text-center animation-fadeIn">
                        <img src="frontend/img/placeholders/screenshots/promo_desktop_left.png" alt="Promo Image 1">
                    </p>
                </div>
            </section>
        </div>
        <div class="item">
            <section class="site-section site-section-light site-section-top themed-background-fire">
                <div class="container">
                    <h1 class="text-center animation-fadeIn360 hidden-xs"><strong>Featuring a Powerful and Flexible layout</strong></h1>
                    <h2 class="text-center animation-fadeIn360 push hidden-xs">Letting you focus on creating your project</h2>
                    <p class="text-center animation-fadeInLeft">
                        <img src="frontend/img/placeholders/screenshots/promo_desktop_right.png" alt="Promo Image 2">
                    </p>
                </div>
            </section>
        </div>
        <div class="item">
            <section class="site-section site-section-light site-section-top themed-background-amethyst">
                <div class="container">
                    <h1 class="text-center animation-hatch hidden-xs"><strong>Fully Responsive and Retina Ready</strong></h1>
                    <h2 class="text-center animation-hatch push hidden-xs">The UI will look great and crisp</h2>
                    <p class="text-center animation-hatch">
                        <img src="frontend/img/placeholders/screenshots/promo_mobile.png" alt="Promo Image 3">
                    </p>
                </div>
            </section>
        </div>
        <div class="item">
            <section class="site-section site-section-light site-section-top themed-background-modern">
                <div class="container">
                    <h1 class="text-center animation-fadeInLeft hidden-xs"><strong>Tons of features are designed &amp; waiting for you</strong></h1>
                    <h2 class="text-center animation-fadeInRight push hidden-xs">Everything you need for your project</h2>
                    <p class="text-center animation-fadeIn360">
                        <img src="frontend/img/placeholders/screenshots/promo_tablet.png" alt="Promo Image 4">
                    </p>
                </div>
            </section>
        </div>
    </div>
    <!-- END Wrapper for slides -->

    <!-- Controls -->
    <a class="left carousel-control" href="#home-carousel" data-slide="prev">
                    <span>
                        <i class="fa fa-chevron-left"></i>
                    </span>
    </a>
    <a class="right carousel-control" href="#home-carousel" data-slide="next">
                    <span>
                        <i class="fa fa-chevron-right"></i>
                    </span>
    </a>
    <!-- END Controls -->
</div>
<!-- END Home Carousel -->

<!-- Action -->
<section class="site-content site-section">
    <div class="container">
        <div class="site-block text-center">
            <a href="" class="btn btn-lg btn-success"><i class="fa fa-shopping-cart"></i> Fazer orçamento</a>
            <a href="" class="btn btn-lg btn-primary"><i class="fa fa-share"></i> Entre em contato</a>
        </div>
        <hr>
    </div>
</section>
<!-- END Action -->

<!-- Promo #1 -->
<section class="site-content site-section site-slide-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <img src="frontend/img/placeholders/screenshots/promo_desktop_left.png" alt="Promo #1" class="img-responsive">
            </div>
            <div class="col-sm-6 col-md-5 col-md-offset-1 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <h3 class="h2 site-heading site-heading-promo"><strong>Criatividade e</strong> Design</h3>
                <p class="promo-content">Soluções criativas para o seu negócio. <a href="features.html">Saiba mais ..</a></p>
            </div>
        </div>
        <hr>
    </div>
</section>
<!-- END Promo #1 -->

<!-- Promo #2 -->
<section class="site-content site-section site-slide-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-5 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <h3 class="h2 site-heading site-heading-promo"><strong>Poderosas</strong> soluções administrativas</h3>
                <p class="promo-content">Soluções personalizadas de gerenciamento de serviços <a href="features.html">Saiba mais..</a></p>
            </div>
            <div class="col-sm-6 col-md-offset-1 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <img src="frontend/img/placeholders/screenshots/promo_desktop_right.png" alt="Promo #2" class="img-responsive">
            </div>
        </div>
        <hr>
    </div>
</section>
<!-- END Promo #2 -->

<!-- Promo #3 -->
<section class="site-content site-section site-slide-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <img src="frontend/img/placeholders/screenshots/promo_tablet.png" alt="Promo #3" class="img-responsive">
            </div>
            <div class="col-sm-6 col-md-5 col-md-offset-1 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <h3 class="h2 site-heading site-heading-promo"><strong>Design</strong> Responsivo</h3>
                <p class="promo-content">Seu site funcionará bem para todos os devices (PC, celulares, tablets) <a href="features.html">Saiba mais..</a></p>
            </div>
        </div>
        <hr>
    </div>
</section>
<!-- END Promo #3 -->

<!-- Promo #4 -->
<section class="site-content site-section site-slide-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-5 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <h3 class="h2 site-heading site-heading-promo"><strong>SEO</strong> Ranking</h3>
                <p class="promo-content">Sites atuais tem que ser desenvolvidos para aparecer nas pesquisas do google <a href="features.html">Saiba mais..</a></p>
            </div>
            <div class="col-sm-6 col-md-offset-1 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <img src="frontend/img/placeholders/screenshots/promo_mobile.png" alt="Promo #4" class="img-responsive">
            </div>
        </div>
        <hr>
    </div>
</section>
<!-- END Promo #4 -->